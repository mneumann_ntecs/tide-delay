use tide::http::{Method, Request, StatusCode, Url};
use tide::Response;

#[async_std::test]
async fn delayed() {
    let mut app = tide::new();
    app.with(tide_delay::DelayMiddleware::new(
        std::time::Duration::from_millis(250),
    ));
    app.at("/")
        .get(|_| async { Ok(Response::new(StatusCode::Ok)) });

    let req = Request::new(Method::Get, Url::parse("http://_/").unwrap());

    let time = std::time::SystemTime::now();

    let res: tide::http::Response = app.respond(req).await.unwrap();

    assert_eq!(res.status(), 200);
    assert!(time.elapsed().unwrap().as_millis() >= 250);
}
