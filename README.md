# tide-delay

A middleware for the Tide web framework that delays responses.

## Example

```rust
let mut app = tide::new();
app.with(tide_delay::DelayMiddleware::new(
    std::time::Duration::from_millis(250),
));
app.at("/")
    .get(|_| async { Ok(Response::new(StatusCode::Ok)) });
```
